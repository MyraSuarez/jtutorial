
public class FirstExercise {

	
	public static void main(String[] args) {
		// first name
		System.out.println("**     **  *     *  *****      * *");
		System.out.println("** * * **    * *    *     *   *   *");
		System.out.println("**  *  **     *     *****    *******");
		System.out.println("**     **     *     *    *   *     *");
		System.out.println("**     **     *     *	  *  *     *");
		System.out.println();
		// middle name
		System.out.println("**     **   * *     ******    * *    *****   *****    * *");
		System.out.println("** * * **  *   *   *         *   *   *    *    *     *   *");
		System.out.println("**  *  ** *******  *  ****  *******  *    *    *    *******");
		System.out.println("**     ** *     *  *     *  *     *  *    *    *    *     *");
		System.out.println("**     ** *     *   *****   *     *  *****   *****  *     *");
		System.out.println();
		// last name
		System.out.println("*******   *     *    * *    *****    *******  *******");
		System.out.println("**        *     *   *   *   *     *  *             *");
		System.out.println("*******   *     *  *******  *****    ****        *");
		System.out.println("     **   *     *  *     *  *    *   *         *");
		System.out.println("*******	   *****   *     *  *	  *  *******  *******");
	
	}

}

