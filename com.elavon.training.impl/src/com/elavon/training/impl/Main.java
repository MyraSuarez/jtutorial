package com.elavon.training.impl;

import com.elavon.training.interF.AwesomeCalcu;

public class Main implements AwesomeCalcu {

	public static void main(String[] args) {
		Main calcu = new Main();
		System.out.println("Sum: " + calcu.getSum(50, 100));
		System.out.println("Difference: " + calcu.getDifference(100, 50));
		System.out.println("Product: " + calcu.getProduct(20, 4));
		System.out.println("Quotient And Remainder: " + calcu.getQuotientAndRemainder(20, 4));
		System.out.println("Sum: " + calcu.toCelsius(20));
		System.out.println("Fahrenheit: " + calcu.toFahrenheit(20));
		System.out.println("Kilogram: " + calcu.toKilogram(20));
        System.out.println("Palindrome: " + calcu.isPalindrome("level")); 

	}

	@Override
	public int getSum(int augend, int addend) {
		// TODO Auto-generated method stub
		return augend+addend;
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		// TODO Auto-generated method stub
		return minuend-subtrahend;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		// TODO Auto-generated method stub
		return multiplicand*multiplier;
	}

	@Override
	public String getQuotientAndRemainder(int dividend, int divisor)
    {
        int remainder = dividend%divisor;
        int quo = dividend/divisor;
        
        String rem = String.valueOf(remainder);
        String quot = String.valueOf(quo);
      
     
        return "the Quotient is: "+ quot +" "+ "The Remainder is:"+ rem;
        
        
    }

    
    public double toCelsius(int fahrenheit)
    
    {
     double cel = (fahrenheit-32)*.556;
    
        return cel;
    }
    
    public double toFahrenheit(int celsius)
    {
        double fah = celsius*1.8+32;
        return fah;
    }
    
    public double toKilogram(double lbs)
    {
        double kg = lbs*0.453592;
    
        return kg;
    }
    
    public double toPound(double kg)
    {
    double lb = kg*2.2;

    return lb;
    }
    
    public boolean isPalindrome(String str)
    {
         int length = str.length();
            if (length < 2) return true;
            return str.charAt(0) != str.charAt(length - 1) ? false :
                    isPalindrome(str.substring(1, length - 1));
        }
        
    }
    
